package uk.me.hoyle.libappnet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import uk.me.hoyle.libappnet.json.PlaceSearchResponse;

import com.google.gson.Gson;

public class PlaceLookup {

	public interface PlaceLookupCallback {
		void onPlacesFound(PlaceSearchResponse places);
		void onPlaceLookupFailed();
	}

	private String mAccessToken;
	
	public PlaceLookup(String accessToken) {
		mAccessToken = accessToken;
	}
	
	public void Search(final double latitude, final double longitude, 
			final float radius, final String query, final int count, 
			final PlaceLookupCallback callback) {
		  new Thread(new Runnable() {
			    public void run() {
					try {
						Gson gson = new Gson();
						String sUrl = "https://alpha-api.app.net/stream/0/places/search"+
								"?latitude="+latitude+"&longitude="+longitude;
						if(radius >= 0) sUrl = sUrl + "&radius="+radius;
						if(query != null && !query.isEmpty()) sUrl = sUrl + "&q="+query;
						if(count >= 0) sUrl = sUrl + "&count="+count;
						
						URL url = new URL(sUrl);
						HttpURLConnection connection = (HttpURLConnection)url.openConnection();
						if(!mAccessToken.isEmpty())
							connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
						InputStreamReader response = new InputStreamReader(connection.getInputStream());
						PlaceSearchResponse place = gson.fromJson(response, PlaceSearchResponse.class);
						connection.disconnect();
						callback.onPlacesFound(place);
					}
					catch(IOException e) {
						e.printStackTrace();
						callback.onPlaceLookupFailed();
					}
			    }
			  }).start();
		}
}
