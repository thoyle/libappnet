package uk.me.hoyle.libappnet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

import uk.me.hoyle.libappnet.json.Stream;
import uk.me.hoyle.libappnet.json.StreamResponse;

import com.google.gson.Gson;

public class StreamManager {
	
	public enum StreamAction {
		eCreated,
		eRetrieved,
		eUpdated,
		eDeleted
	}
	
	public interface StreamCallback {
		void onStreamResult(StreamAction action, StreamResponse stream);
		void onStreamError();
	}

	private String mAccessToken;

	public StreamManager(String accessToken) {
		mAccessToken = accessToken;
	}
	
	public void Create(final Stream streamDescriptor, final StreamCallback callback) {
		  new Thread(new Runnable() {
			    public void run() {
					try {
						Gson gson = new Gson();
						URL url = new URL("https://alpha-api.app.net/stream/0/streams");
						HttpURLConnection connection = (HttpURLConnection)url.openConnection();
						if(!mAccessToken.isEmpty())
							connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
						connection.setRequestProperty("Content-Type", "application/json");
						connection.setRequestMethod("POST");
						PrintStream ps = new PrintStream(connection.getOutputStream());
						ps.print(gson.toJson(streamDescriptor));
						ps.close();
						InputStreamReader response = new InputStreamReader(connection.getInputStream());
						StreamResponse stream = gson.fromJson(response, StreamResponse.class);
						connection.disconnect();
						callback.onStreamResult(StreamAction.eCreated, stream);
					}
					catch(IOException e) {
						e.printStackTrace();
						callback.onStreamError();
					}
			    }
		}).start();
	}
	
	public void Retrieve(final String streamId, final StreamCallback callback) {
		  new Thread(new Runnable() {
			    public void run() {
					try {
						Gson gson = new Gson();
						URL url = new URL("https://alpha-api.app.net/stream/0/streams/"+streamId);
						HttpURLConnection connection = (HttpURLConnection)url.openConnection();
						if(!mAccessToken.isEmpty())
							connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
						connection.setRequestMethod("GET");
						InputStreamReader response = new InputStreamReader(connection.getInputStream());
						StreamResponse stream = gson.fromJson(response, StreamResponse.class);
						connection.disconnect();
						callback.onStreamResult(StreamAction.eRetrieved, stream);
					}
					catch(IOException e) {
						e.printStackTrace();
						callback.onStreamError();
					}
			    }
		}).start();
	}
	
	public void Update(final Stream streamDescriptor, final StreamCallback callback) {
		  new Thread(new Runnable() {
			    public void run() {
					try {
						Gson gson = new Gson();
						URL url = new URL("https://alpha-api.app.net/stream/0/stream/"+streamDescriptor.id);
						HttpURLConnection connection = (HttpURLConnection)url.openConnection();
						if(!mAccessToken.isEmpty())
							connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
						connection.setRequestProperty("Content-Type", "application/json");
						connection.setRequestMethod("PUT");
						PrintStream ps = new PrintStream(connection.getOutputStream());
						ps.print(gson.toJson(streamDescriptor));
						ps.close();
						InputStreamReader response = new InputStreamReader(connection.getInputStream());
						StreamResponse stream = gson.fromJson(response, StreamResponse.class);
						connection.disconnect();
						callback.onStreamResult(StreamAction.eUpdated, stream);
					}
					catch(IOException e) {
						e.printStackTrace();
						callback.onStreamError();
					}
			    }
		}).start();
	}

	public void Delete(final String streamId, final StreamCallback callback) {
		  new Thread(new Runnable() {
			    public void run() {
					try {
						Gson gson = new Gson();
						URL url = new URL("https://alpha-api.app.net/stream/0/streams/"+streamId);
						HttpURLConnection connection = (HttpURLConnection)url.openConnection();
						if(!mAccessToken.isEmpty())
							connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
						connection.setRequestMethod("DELETE");
						InputStreamReader response = new InputStreamReader(connection.getInputStream());
						StreamResponse stream = gson.fromJson(response, StreamResponse.class);
						connection.disconnect();
						callback.onStreamResult(StreamAction.eDeleted, stream);
					}
					catch(IOException e) {
						e.printStackTrace();
						callback.onStreamError();
					}
			    }
		}).start();
	}
}

