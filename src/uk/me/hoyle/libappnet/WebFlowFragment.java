package uk.me.hoyle.libappnet;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@SuppressLint("SetJavaScriptEnabled")
public class WebFlowFragment extends Fragment {
	
	private static final String REDIRECT_URL = "http://localhost/";
	private static final String OAUTH_URL = "https://account.app.net/oauth/authenticate";
	
	private String mClientId;
	private String mScope;
	private WebFlowAuth.AuthCallback mCallback;

	static WebFlowFragment Create(String clientId, String scope, WebFlowAuth.AuthCallback callback) {
		
		WebFlowFragment result = new WebFlowFragment();
		result.mClientId = clientId;
		result.mScope = scope;
		result.mCallback = callback;
		return result;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
				
		WebView webview = (WebView)inflater.inflate(R.layout.webview, container, false);
		
		final Activity activity = getActivity();
		
		webview.getSettings().setJavaScriptEnabled(true);
		
		webview.setWebChromeClient(new WebChromeClient() {
			   public void onProgressChanged(WebView view, int progress) {				   
				 activity.setProgress(progress * 1000);
			   }
			 });
		
		 webview.setWebViewClient(new WebViewClient() {
		   public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			   if(failingUrl.contains(REDIRECT_URL)) {
				   URL url;
				   String[] tokens;
				   String access_token = "", error = "", error_description = "";
				  
				   try {
					   url = new URL(failingUrl);
					   tokens = url.getRef().split("&");
				   } catch (MalformedURLException e) {
						tokens = new String[1];
						tokens[0]="error=failed";
				   }
				   for(String token: tokens) {
					   try {
						   if(token.contains("access_token=")) {
							   access_token = URLDecoder.decode(token.replaceFirst("access_token=", ""),"UTF-8");
						   } else if(token.contains("error=")) {
							   error = URLDecoder.decode(token.replaceFirst("error=", ""),"UTF-8");
						   } else if(token.contains("error_description=")) {
								error_description = URLDecoder.decode(token.replaceFirst("error_description=", ""),"UTF-8");
						   }	
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
				   }
				   if(mCallback != null) {
					   if(!access_token.isEmpty()) {
						   mCallback.onAuthenticated(access_token);						   							   
					   } else {
						   mCallback.onFailed(error, error_description);						   						   							   
					   }
				   }
				   getActivity().getSupportFragmentManager().popBackStack();
			   }
		   }
		});
		 
		String url = OAUTH_URL +
				"?client_id=" + mClientId +
				"&response_type=token" +
				"&redirect_url=" + REDIRECT_URL + 
				"&scope=" + mScope;
				
		webview.loadUrl(url);
		
		return webview;
	}	
}
