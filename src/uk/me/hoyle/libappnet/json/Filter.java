package uk.me.hoyle.libappnet.json;

import java.util.List;

public class Filter {
	public static class Clause {
		public String object_type;
		public String operator;
		public String field;
		public String value;
	}
	
	public String id;
	public String name;
	public List<Clause> clauses;
	public String match_policy;
}
