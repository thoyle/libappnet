package uk.me.hoyle.libappnet.json;

public class FilterResponse {

	public class Meta {
		public String code;		
		public String error;
		public String error_description;
		public String error_uri;		
	}
	
	public Stream data;
	public Meta meta;
		
}
