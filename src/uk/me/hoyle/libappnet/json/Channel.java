package uk.me.hoyle.libappnet.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Channel {
	public static class Acl {
		public boolean any_user;
		public boolean immutable;
		@SerializedName("public") public boolean is_public;
		public List<String> user_ids;
		public boolean you;
	}
	
	public static class Counts {
		public int messages;
	}
	
	public Counts counts;
	public String id;
	public String type;
	public String user;
	public List<Annotation> annotations;
	public Acl readers;
	public Acl writers;
	public boolean you_muted;
	public boolean you_subscribed;
	public boolean you_can_edit;
	public boolean has_unread;
	public StreamMarker marker;
	public String recent_message_id;
	public Message recent_message;
}
