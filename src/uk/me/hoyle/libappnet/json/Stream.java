package uk.me.hoyle.libappnet.json;

import java.util.List;

public class Stream {
	public String id;
	public String endpoint;
	public Filter filter;
	public List<String> object_types;
	public String type;
	public String key;
}
