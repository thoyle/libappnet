package uk.me.hoyle.libappnet.json;

import java.util.List;

public class User {
	public class Image {
		public int width;
		public int height;
		public String url;
		public boolean is_default;
	}

	public class Description {
		public String text;
		public String html;
		public Entity entities;			
	}

	public class Counts {
		public int following;
		public int followers;
		public int posts;
		public int stars;			
	}

	public String id;
	public String username;
	Description description;
	public String timezone;
	public String locale;
	public Image avatar_image;
	public Image cover_image;
	public String type;
	public String canonical_url;
	public String created_at;
	public boolean follows_you;
	public boolean you_follow;
	public boolean you_muted;
	public boolean you_can_subscribe;
	public List<Annotation> annotations;		
}
	