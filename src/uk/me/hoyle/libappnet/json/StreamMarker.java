package uk.me.hoyle.libappnet.json;

public class StreamMarker {
	public String id;
	public String last_read_id;
	public String name;
	public int percentage;
	public String updated_at;
	public String version;
}
