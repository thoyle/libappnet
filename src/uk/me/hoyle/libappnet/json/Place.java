package uk.me.hoyle.libappnet.json;

import java.util.List;

public class Place {
	public static class Category {
		public List<String> labels;
		public String id;
	}
	
	public String factual_id;
	public String name;
	public String address;
	public String address_extended;
	public String locality;
	public String region;
	public String admin_region;
	public String post_town;
	public String po_box;
	public String postcode;
	public String country_code;
	public double latitude;
	public double longitude;
	public String is_open;
	public String telephone;
	public String fax;
	public String website;
	public List<Category> categories;
}
