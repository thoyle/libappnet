package uk.me.hoyle.libappnet.json;

import java.util.List;

public class Entity {

	public class Mentions {
		public String name;
		public String id;
		public int pos;
		public int len;
	}
	
	public class Hashtags {
		public String name;
		public int pos;
		public int len;
	}
	
	public class Links {
		public String text;
		public String url;
		public int pos;
		public int len;
		public int amended_len;
	}
	
	public List<Mentions> mensions;
	public List<Hashtags> hashtags;
	public List<Links> links;
}
