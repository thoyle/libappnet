package uk.me.hoyle.libappnet.json;

import java.util.List;

public class Message {
	public String id;
	public String channel_id;
	public String user;
	public String created_at;
	public String text;
	public String html;
	public Object source;
	public String reply_to;
	public String thread_id;
	public int num_replies;
	public List<Annotation> annotations;
	public Entity entities;
	public boolean is_deleted;
	public boolean machine_only;
}
