package uk.me.hoyle.libappnet.json;

import java.util.List;

public class PlaceSearchResponse {
	
	public class Meta {
		public String code;		
		public String error;
		public String error_description;
		public String error_uri;		
	}
	
	public List<Place> data;
	public Meta meta;
}
