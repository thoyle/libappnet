package uk.me.hoyle.libappnet.json;

public class UserResponse {
	
	public class Meta {
		public String code;		
		public String error;
		public String error_description;
		public String error_uri;		
	}
	
	public User data;
	public Meta meta;
}
