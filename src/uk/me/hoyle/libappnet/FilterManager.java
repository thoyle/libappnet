package uk.me.hoyle.libappnet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

import uk.me.hoyle.libappnet.json.Filter;
import uk.me.hoyle.libappnet.json.FilterResponse;

import com.google.gson.Gson;

public class FilterManager {
	
	public enum FilterAction {
		eCreated,
		eRetrieved,
		eUpdated,
		eDeleted
	}
	
	public interface FilterCallback {
		void onFilterResult(FilterAction action, FilterResponse filter);
		void onFilterError();
	}

	private String mAccessToken;

	public FilterManager(String accessToken) {
		mAccessToken = accessToken;
	}
	
	public void Create(final Filter filterDescriptor, final FilterCallback callback) {
		  new Thread(new Runnable() {
			    public void run() {
					try {
						Gson gson = new Gson();
						URL url = new URL("https://alpha-api.app.net/stream/0/filters");
						HttpURLConnection connection = (HttpURLConnection)url.openConnection();
						if(!mAccessToken.isEmpty())
							connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
						connection.setRequestProperty("Content-Type", "application/json");
						connection.setRequestMethod("POST");
						PrintStream ps = new PrintStream(connection.getOutputStream());
						ps.print(gson.toJson(filterDescriptor));
						ps.close();
						InputStreamReader response = new InputStreamReader(connection.getInputStream());
						FilterResponse filter = gson.fromJson(response, FilterResponse.class);
						connection.disconnect();
						callback.onFilterResult(FilterAction.eCreated, filter);
					}
					catch(IOException e) {
						e.printStackTrace();
						callback.onFilterError();
					}
			    }
		}).start();
	}

	public void Retrieve(final String filterId, final FilterCallback callback) {
		  new Thread(new Runnable() {
			    public void run() {
					try {
						Gson gson = new Gson();
						URL url = new URL("https://alpha-api.app.net/stream/0/filters/"+filterId);
						HttpURLConnection connection = (HttpURLConnection)url.openConnection();
						if(!mAccessToken.isEmpty())
							connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
						connection.setRequestMethod("GET");
						InputStreamReader response = new InputStreamReader(connection.getInputStream());
						FilterResponse filter = gson.fromJson(response, FilterResponse.class);
						connection.disconnect();
						callback.onFilterResult(FilterAction.eRetrieved, filter);
					}
					catch(IOException e) {
						e.printStackTrace();
						callback.onFilterError();
					}
			    }
		}).start();
	}

	public void Update(final Filter filterDescriptor, final FilterCallback callback) {
		  new Thread(new Runnable() {
			    public void run() {
					try {
						Gson gson = new Gson();
						URL url = new URL("https://alpha-api.app.net/stream/0/filter/"+filterDescriptor.id);
						HttpURLConnection connection = (HttpURLConnection)url.openConnection();
						if(!mAccessToken.isEmpty())
							connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
						connection.setRequestProperty("Content-Type", "application/json");
						connection.setRequestMethod("PUT");
						PrintStream ps = new PrintStream(connection.getOutputStream());
						ps.print(gson.toJson(filterDescriptor));
						ps.close();
						InputStreamReader response = new InputStreamReader(connection.getInputStream());
						FilterResponse filter = gson.fromJson(response, FilterResponse.class);
						connection.disconnect();
						callback.onFilterResult(FilterAction.eUpdated, filter);
					}
					catch(IOException e) {
						e.printStackTrace();
						callback.onFilterError();
					}
			    }
		}).start();
	}

	public void Delete(final String filterId, final FilterCallback callback) {
		  new Thread(new Runnable() {
			    public void run() {
					try {
						Gson gson = new Gson();
						URL url = new URL("https://alpha-api.app.net/stream/0/filters/"+filterId);
						HttpURLConnection connection = (HttpURLConnection)url.openConnection();
						if(!mAccessToken.isEmpty())
							connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
						connection.setRequestMethod("DELETE");
						InputStreamReader response = new InputStreamReader(connection.getInputStream());
						FilterResponse filter = gson.fromJson(response, FilterResponse.class);
						connection.disconnect();
						callback.onFilterResult(FilterAction.eUpdated, filter);
					}
					catch(IOException e) {
						e.printStackTrace();
						callback.onFilterError();
					}
			    }
		}).start();
	}
}
