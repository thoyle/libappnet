package uk.me.hoyle.libappnet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import uk.me.hoyle.libappnet.json.UserResponse;

import com.google.gson.Gson;

public class UserLookup {
	
	public interface UserLookupCallback {
		void onUserFound(UserResponse user);
		void onUserLookupFailed();
	}

	private String mAccessToken;
	
	public UserLookup(String accessToken) {
		mAccessToken = accessToken;
	}
	
	public void Lookup(final String userId, final UserLookupCallback callback) {
	  new Thread(new Runnable() {
		    public void run() {
				try {
					Gson gson = new Gson();
					URL url = new URL("https://alpha-api.app.net/stream/0/users/"+userId);
					HttpURLConnection connection = (HttpURLConnection)url.openConnection();
					if(!mAccessToken.isEmpty())
						connection.addRequestProperty("Authorization", "Bearer "+mAccessToken);
					InputStreamReader response = new InputStreamReader(connection.getInputStream());
					UserResponse user = gson.fromJson(response, UserResponse.class);
					connection.disconnect();
					callback.onUserFound(user);
				}
				catch(IOException e) {
					e.printStackTrace();
					callback.onUserLookupFailed();
				}
		    }
		  }).start();
	}
}
