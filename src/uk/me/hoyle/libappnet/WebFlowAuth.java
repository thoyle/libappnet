package uk.me.hoyle.libappnet;

import java.util.Set;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class WebFlowAuth {
	
	public interface AuthCallback {
		void onAuthenticated(String token);
		void onFailed(String error, String description);
	}

	public final static int AUTH_TOKEN = 1001;
	
	public enum scopeType {
			scopeBasic,
			scopeStream,
			scopeEmail,
			scopeWritePost,
			scopeFollow,
			scopePublicMessages,
			scopeMessages,
			scopeUpdateProfile,
			scopeFiles,
			scopeExport
	};
	
	public WebFlowAuth(FragmentActivity context, int parent_id, String clientId, Set<scopeType> scopeList, AuthCallback callback) {
		
		StringBuilder s = new StringBuilder();
		
		s.append("basic");
		for(scopeType scopeEnum : scopeList) {
			switch(scopeEnum) {
			case scopeBasic:
				// Basic is always granted
				break;
			case scopeStream:
				s.append(",stream");
				break;
			case scopeEmail:
				s.append(",email");
				break;
			case scopeWritePost:
				s.append(",write_post");
				break;
			case scopeFollow:
				s.append(",follow");
				break;
			case scopePublicMessages:
				s.append(",public_messages");
				break;
			case scopeMessages:
				s.append(",messages");
				break;
			case scopeUpdateProfile:
				s.append(",update_profile");
				break;
			case scopeFiles:
				s.append(",files");
				break;
			case scopeExport:	
				s.append(",export");
				break;
			}
		}
		
		String scope = s.toString();

		FragmentManager manager = context.getSupportFragmentManager();
	    WebFlowFragment web = WebFlowFragment.Create(clientId, scope, callback);
	    
	    manager.beginTransaction().add(parent_id, web).addToBackStack(null).commit();
	}
}
